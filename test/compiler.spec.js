/* global: eighteen, describe, beforeEach, afterEach, it, chai */
(function (eighteen, chai) {
    var assert = chai.assert;
    var compile = eighteen.compile;

    describe("Compiler", function () {

        afterEach(function () {
            compile.disposeMarkups();
        });

        it("can compile a placeholder", function () {
            var compiled = compile("${ a }", {
                a: "a"
            });

            assert.equal(compiled, "a");
        });

        it("can compile a template with a simple map", function () {
            var compiled = compile("a ${ a } b ${b} c ${  c  } end", {
                a: "a",
                b: "b",
                c: "c"
            });

            assert.equal(compiled, "a a b b c c end");
        });

        it("can compile a template with a nested map", function () {
            var compiled = compile("a ${ moo.aye } b ${ moo.bii.bee } c ${ moo.see.ya.there }", {
                moo: {
                    aye: "a",
                    bii: {
                        bee: "b"
                    },
                    see: {
                        ya: {
                            there: "c"
                        }
                    }
                }
            });

            assert.equal(compiled, "a a b b c c");
        });

        it("can cache templates", function () {
            var template = "a ${ a }";
            compile(template, {
                a: "a"
            });

            assert.ok(compile.getCompiled(template));
        });

        it("will not cache templates without placeholder", function () {
            var template = "hakuna matata";

            compile(template);

            assert.notOk(compile.getCompiled(template));
        });

        it("can pre-compile a template but not compile it", function () {
            var compiled = compile("${ a }");
            assert.notOk(compiled);
        });

        it("can dispose the cached templates", function () {
            var template = "a ${ a }";
            compile(template, {
                a: "a"
            });

            compile.disposeMarkups();

            assert.notOk(compile.getCompiled(template));
        });

    });

}(eighteen, chai));