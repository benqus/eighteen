/* global: eighteen, describe, beforeEach, afterEach, it, chai */
(function (eighteen, chai) {
    var assert = chai.assert;

    describe("I18n", function () {

        var dictionary = {
            a: "a",
            b: "${placeholder}",
            c: {
                d: "d",
                e: {
                    f: "${placeholder}"
                }
            }
        };

        afterEach(function () {
            eighteen.forgetAll();
        });

        it("can register a dictionary", function () {
            eighteen.add("a", dictionary);

            assert.equal(eighteen.get("a"), dictionary);
        });

        it("can forget a dictionary", function () {
            eighteen.add("a", dictionary);
            eighteen.forget("a");

            assert.notOk(eighteen.get("a"));
        });

        // String
        it("can translate a shallow dictionary with a String path", function () {
            eighteen.add("dictionary", dictionary);

            assert.equal(eighteen("dictionary", "a"), dictionary.a);
        });

        it("can translate and compile a shallow dictionary with a String path", function () {
            var result;

            eighteen.add("dictionary", dictionary);

            result = eighteen("dictionary", "b", {
                placeholder: "b"
            });

            assert.equal(result, "b");
        });

        it("can translate a nested dictionary with a String path", function () {
            eighteen.add("dictionary", dictionary);

            assert.equal(eighteen("dictionary", "c.d"), dictionary.c.d);
        });

        it("can translate and compile a nested dictionary with a String path", function () {
            var result;

            eighteen.add("dictionary", dictionary);

            result = eighteen("dictionary", "c.e.f", {
                placeholder: "f"
            });

            assert.equal(result, "f");
        });

        // Array
        it("can translate a shallow dictionary with an Array path", function () {
            eighteen.add("dictionary", dictionary);

            assert.equal(eighteen("dictionary", ["a"]), dictionary.a);
        });

        it("can translate and compile a shallow dictionary with an Array path", function () {
            var result;

            eighteen.add("dictionary", dictionary);

            result = eighteen("dictionary", ["b"], {
                placeholder: "b"
            });

            assert.equal(result, "b");
        });

        it("can translate a nested dictionary with an Array path", function () {
            eighteen.add("dictionary", dictionary);

            assert.equal(eighteen("dictionary", ["c", "d"]), dictionary.c.d);
        });

        it("can translate and compile a nested dictionary with an Array path", function () {
            var result;

            eighteen.add("dictionary", dictionary);

            result = eighteen("dictionary", ["c", "e", "f"], {
                placeholder: "f"
            });

            assert.equal(result, "f");
        });

    });

}(eighteen, chai));