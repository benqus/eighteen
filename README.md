eighteen
===

Minimal dictionary (i18n) & template utility

---

### Adding dictionaries

    eighteen
        .add("en_EN", {
            popup: {
                title: "You are deleting ${files.count} files. Are you sure?"
            }
        })
        .add("hu_HU", {
            popup: {
                title: "Törölni fogsz ${files.count} fájlt. Biztos?"
            }
        });

---

### Using already added dictionaries

    var options = {
        files: {
            count: 5
        }
    };

    var translatedTitle = eighteen("en_EN", "popup.title", options);

    console.log(translatedTitle); // "You are deleting 5 files. Are you sure?"

---

### License

MIT