/* global: eighteen */
(function (eighteen) {

    /**
     * Lookup of precompiled templates.
     * @static
     * @private
     * @type {Object}
     */
    var compiledTemplates = {};

    /**
     * RegExp to find the placeholders inside the templates.
     * @static
     * @private
     * @type {RegExp}
     */
    var regExp = /\$\{\s*[\w|\.]+\s*\}/gim;

    /**
     * Memoizes a method for a placeholder to access attributes.
     * @static
     * @private
     * @param {String} placeholder - eg: "${ myPlaceholder }"
     * @type {Function}
     * @returns {Function}
     */
    var compilerMethodFactory = function (placeholder) {
        var length = (placeholder.length - 1);
        var sanitized = placeholder.substring(2, length).trim().split(".");
        var l = sanitized.length;

        /**
         * Discovers the nested/attribute to fetch from attribute passed.
         * @param {Object} map - attributes
         * @returns {String}
         */
        return function (map) {
            var result = map[sanitized[0]];
            var ns = map;
            var i = 0;

            // digging down the namespace
            if (l > 1) {
                while (ns && i < l) {
                    ns = ns[sanitized[i++]];
                }

                result = ns;
            }

            return result;
        };
    };

    /**
     * Generates a compiled, cache-able template to reuse.
     * @static
     * @private
     * @param {String} text - eg: "hakuna ${ timon } matata"
     * @type {Function}
     * @returns {Array}
     */
    var generateCompiledResult = function (text) {
        // placeholders to replace
        var placeholders = text.match(regExp);
        var i = 0;
        var splitText, compiledResults;

        if (placeholders) {
            // actual template content
            splitText = text.split(regExp);

            // precompiled array of content
            compiledResults = [];

            while (placeholders.length > 0) {
                compiledResults.push(
                    splitText[i],
                    compilerMethodFactory(placeholders.shift())
                );

                i += 1;
            }

            compiledResults.push(splitText[i]);

            compiledTemplates[text] = compiledResults;
        }

        return (compiledTemplates[text] || text);
    };

    /**
     * Pre-compiles and caches the given template.
     * If attributes is defined, it will compile the template into a String.
     * @static
     * @private
     * @param {String} text - eg: "hakuna ${ timon } matata"
     * @param {Object} [attrs] - map of attributes to render
     * @returns {String}
     */
    var compile = function (text, attrs) {
        var compiledTemplate = compiledTemplates[text];
        var markup = "";
        var item, i, l;

        if (!compiledTemplate) {
            compiledTemplate = generateCompiledResult(text);
        }

        // if a map of key-value pairs is provided, compile too
        if (attrs && typeof attrs === 'object') {
            for (i = 0, l = compiledTemplate.length; i < l; i++) {
                item = compiledTemplate[i];
                markup += (typeof item === 'function' ? item(attrs) : item);
            }
        }

        return markup;
    };

    /**
     * Returns the specified comiled markups.
     * @public
     * @static
     * @param {String} template - eg: "hakuna ${ timon } matata"
     * @returns {String|undefined}
     */
    compile.getCompiled = function (template) {
        return compiledTemplates[template];
    };

    /**
     * Disposes all previously compiled and cached markups.
     * @public
     * @static
     */
    compile.disposeMarkups = function () {
        compiledTemplates = {};
    };

    eighteen.compile = compile;

}(eighteen));