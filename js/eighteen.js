/**
 * @namespace eighteen
 * @type {Function}
 */
var eighteen = (function () {

    /**
     * @static
     * @private
     * @namespace dictionaries
     * @type {Object}
     */
    var dictionaries = {};

    /**
     * @private
     * @static
     * @param {Object} dictionary - dictionary
     * @param {Array} path - path to resolve
     * @returns {String|undefined}
     */
    var resolveDictionaryEntry = function (dictionary, path) {
        var namespace = dictionary;
        var entry = path.pop();

        while (namespace && path.length > 0) {
            namespace = namespace[path.shift()];
        }

        if (namespace) {
            return namespace[entry];
        }
    };

    /**
     * Returns the compiled translation form the required dictionary
     * @param {String} dict - dictionary to translate for
     * @param {String} path - dictionary entry namespace
     * @param {Object} [attrs] - attributes to compile the entry with
     * @return {String|undefined}
     */
    var eighteen = function (dict, path, attrs) {
        var dictionary = eighteen.get(dict);
        var result = "";
        var _path;

        if (dictionary && path) {
            // resolve path
            if (typeof path === "string") {
                _path = path.split(".");
            } else if (path instanceof Array) {
                _path = path.slice();
            }

            // make sure that _path is an Array but do not fail
            if (_path instanceof Array) {
                result = resolveDictionaryEntry(dictionary, _path);

                // do not compile if there are no attributes provided
                if (attrs && typeof attrs === 'object') {
                    result = eighteen.compile(result, attrs);
                }
            }
        }

        return result;
    };

    /**
     * Returns the required dictionary if registered.
     * @param {String} dict - dictionary
     * @returns {Object|undefined}
     */
    eighteen.get = function (dict) {
        return dictionaries[dict];
    };

    /**
     * Adds a new dictionary.
     * @param {String} name - name of the dictionary
     * @param {Object} dict - dictionary to add
     */
    eighteen.add = function (name, dict) {
        if (typeof name !== 'string') {
            throw new TypeError("Invalid name for dictionary!");
        }
        if (dictionaries[name]) {
            throw new TypeError("Dictionary is already in use!");
        }
        if (!dict || typeof dict !== 'object') {
            throw new TypeError("Invalid dictionary!");
        }

        dictionaries[name] = dict;

        return this;
    };

    /**
     * Forgets a dictionary
     * @param {String} dictionary
     */
    eighteen.forget = function (dictionary) {
        delete dictionaries[dictionary];
    };

    /**
     * Forgets all dictionaries
     */
    eighteen.forgetAll = function () {
        var i;
        for (i in dictionaries) {
            if (dictionaries.hasOwnProperty(i)) {
                delete dictionaries[i];
            }
        }
    };

    return eighteen;

}());