module.exports = function (grunt) {
    var pkg = grunt.file.readJSON("package.json");

    grunt.initConfig({
        pkg: pkg,

        mocha: {
            test: {
                src: ['test/runner.html']
            }
        },

        concat: {
            options: {
                banner: "" +
                    "/**\n" +
                    " * Eighteen <%= pkg.version %> \n" +
                    " * URL: https://bitbucket.org/benqus/eighteen \n" +
                    " */\n\n"
            },
            build: {
                src: [
                    'js/eighteen.js',
                    'js/compiler.js'
                ],
                dest: 'dist/eighteen.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-mocha');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('test', ['mocha:test']);
    grunt.registerTask('build', ['mocha:test', 'concat:build']);
};