/**
 * Eighteen 0.0.4 
 * URL: https://bitbucket.org/benqus/eighteen 
 */

/**
 * @namespace eighteen
 * @type {Function}
 */
var eighteen = (function () {

    /**
     * @static
     * @private
     * @namespace dictionaries
     * @type {Object}
     */
    var dictionaries = {};

    /**
     * @private
     * @static
     * @param {Object} dictionary - dictionary
     * @param {Array} path - path to resolve
     * @returns {String|undefined}
     */
    var resolveDictionaryEntry = function (dictionary, path) {
        var namespace = dictionary;
        var entry = path.pop();

        while (namespace && path.length > 0) {
            namespace = namespace[path.shift()];
        }

        if (namespace) {
            return namespace[entry];
        }
    };

    /**
     * Returns the compiled translation form the required dictionary
     * @param {String} dict - dictionary to translate for
     * @param {String} path - dictionary entry namespace
     * @param {Object} [attrs] - attributes to compile the entry with
     * @return {String|undefined}
     */
    var eighteen = function (dict, path, attrs) {
        var dictionary = eighteen.get(dict);
        var result = "";
        var _path;

        if (dictionary && path) {
            // resolve path
            if (typeof path === "string") {
                _path = path.split(".");
            } else if (path instanceof Array) {
                _path = path.slice();
            }

            // make sure that _path is an Array but do not fail
            if (_path instanceof Array) {
                result = resolveDictionaryEntry(dictionary, _path);

                // do not compile if there are no attributes provided
                if (attrs && typeof attrs === 'object') {
                    result = eighteen.compile(result, attrs);
                }
            }
        }

        return result;
    };

    /**
     * Returns the required dictionary if registered.
     * @param {String} dict - dictionary
     * @returns {Object|undefined}
     */
    eighteen.get = function (dict) {
        return dictionaries[dict];
    };

    /**
     * Adds a new dictionary.
     * @param {String} name - name of the dictionary
     * @param {Object} dict - dictionary to add
     */
    eighteen.add = function (name, dict) {
        if (typeof name !== 'string') {
            throw new TypeError("Invalid name for dictionary!");
        }
        if (dictionaries[name]) {
            throw new TypeError("Dictionary is already in use!");
        }
        if (!dict || typeof dict !== 'object') {
            throw new TypeError("Invalid dictionary!");
        }

        dictionaries[name] = dict;

        return this;
    };

    /**
     * Forgets a dictionary
     * @param {String} dictionary
     */
    eighteen.forget = function (dictionary) {
        delete dictionaries[dictionary];
    };

    /**
     * Forgets all dictionaries
     */
    eighteen.forgetAll = function () {
        var i;
        for (i in dictionaries) {
            if (dictionaries.hasOwnProperty(i)) {
                delete dictionaries[i];
            }
        }
    };

    return eighteen;

}());
/* global: eighteen */
(function (eighteen) {

    /**
     * Lookup of precompiled templates.
     * @static
     * @private
     * @type {Object}
     */
    var compiledTemplates = {};

    /**
     * RegExp to find the placeholders inside the templates.
     * @static
     * @private
     * @type {RegExp}
     */
    var regExp = /\$\{\s*[\w|\.]+\s*\}/gim;

    /**
     * Memoizes a method for a placeholder to access attributes.
     * @static
     * @private
     * @param {String} placeholder - eg: "${ myPlaceholder }"
     * @type {Function}
     * @returns {Function}
     */
    var compilerMethodFactory = function (placeholder) {
        var length = (placeholder.length - 1);
        var sanitized = placeholder.substring(2, length).trim().split(".");
        var l = sanitized.length;

        /**
         * Discovers the nested/attribute to fetch from attribute passed.
         * @param {Object} map - attributes
         * @returns {String}
         */
        return function (map) {
            var result = map[sanitized[0]];
            var ns = map;
            var i = 0;

            // digging down the namespace
            if (l > 1) {
                while (ns && i < l) {
                    ns = ns[sanitized[i++]];
                }

                result = ns;
            }

            return result;
        };
    };

    /**
     * Generates a compiled, cache-able template to reuse.
     * @static
     * @private
     * @param {String} text - eg: "hakuna ${ timon } matata"
     * @type {Function}
     * @returns {Array}
     */
    var generateCompiledResult = function (text) {
        // placeholders to replace
        var placeholders = text.match(regExp);
        var i = 0;
        var splitText, compiledResults;

        if (placeholders) {
            // actual template content
            splitText = text.split(regExp);

            // precompiled array of content
            compiledResults = [];

            while (placeholders.length > 0) {
                compiledResults.push(
                    splitText[i],
                    compilerMethodFactory(placeholders.shift())
                );

                i += 1;
            }

            compiledResults.push(splitText[i]);

            compiledTemplates[text] = compiledResults;
        }

        return (compiledTemplates[text] || text);
    };

    /**
     * Pre-compiles and caches the given template.
     * If attributes is defined, it will compile the template into a String.
     * @static
     * @private
     * @param {String} text - eg: "hakuna ${ timon } matata"
     * @param {Object} [attrs] - map of attributes to render
     * @returns {String}
     */
    var compile = function (text, attrs) {
        var compiledTemplate = compiledTemplates[text];
        var markup = "";
        var item, i, l;

        if (!compiledTemplate) {
            compiledTemplate = generateCompiledResult(text);
        }

        // if a map of key-value pairs is provided, compile too
        if (attrs && typeof attrs === 'object') {
            for (i = 0, l = compiledTemplate.length; i < l; i++) {
                item = compiledTemplate[i];
                markup += (typeof item === 'function' ? item(attrs) : item);
            }
        }

        return markup;
    };

    /**
     * Returns the specified comiled markups.
     * @public
     * @static
     * @param {String} template - eg: "hakuna ${ timon } matata"
     * @returns {String|undefined}
     */
    compile.getCompiled = function (template) {
        return compiledTemplates[template];
    };

    /**
     * Disposes all previously compiled and cached markups.
     * @public
     * @static
     */
    compile.disposeMarkups = function () {
        compiledTemplates = {};
    };

    eighteen.compile = compile;

}(eighteen));